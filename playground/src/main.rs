
const OUR_COURSE: &str = "Rust with AutoGPT";

fn main() {
    println!("Welcome to this course on {}!", OUR_COURSE);

    // stack 
    // let x: i32;
    // x = 2;
    // println!("x is {}", x);

    // let y: i32 = 4;
    // println!("y is {}", y);

    // // for loop
    // for i in 0..=y {
    //     if i != 4{
    //         println!("{}", i);
    //     } else {
    //         println!("{}", i);
    //     }
    // }

    // let mut z: i32 = 5;
    // println!("Z was {}", z);
    // z  = 10;
    // println!("but is now {}", z);

    // let freezing_temp: f64 = -2.4;
    // println!("Freezing temp is {}", freezing_temp);

    // let is_zero_remainder: bool = 10 % 4 != 0;
    // println!("is_zero_remainder: {}", is_zero_remainder);

    // let my_char: char = 'z';
    // println!("The character for my char is : {}", my_char);

    // let my_floats: [f32; 10] = [0.0; 10];
    // println!("my_ints is {:?}", my_floats);

    // let my_floats_new: [f32; 10] = my_floats.map(|n| n + 2.0);
    // println!("my_floats_new is {:?}", my_floats_new);

    // dynamic size variables and collections

    // let name: &str = "Mthandazo";
    // println!("Name is {}", name);

    // let dynamic_name: String = String::from("Mthandazo Ndhlovu");
    // println!("dynamic_name is {:?}", dynamic_name);
    // println!("my name dynamic_name store in memory: {:p}", &dynamic_name);

    // let str_slice: &str = &dynamic_name[0..5];
    // println!("str_slice is {}", str_slice);

    // let mut chars: Vec<char> = Vec::new();
    // chars.insert(0, 'h');
    // chars.insert(1, 'e');
    // chars.insert(2, 'l');
    // chars.push('l');
    // chars.push('o');
    // chars.push('.');
    // println!("char is {:?}", chars);

    // let removed_char: char = chars.pop().unwrap();
    // println!("removed_char is {:?}", removed_char);
    // println!("chars is {:?}", chars);

    // chars.iter().for_each(|c|println!("char is {}", c));

    // let chars_again: Vec<char> = vec!('h','e','l','l','o');
    // println!("chars_again: {:?}", chars_again);

    // let collected: String = chars_again.iter().collect();
    // println!("Collected: {:?}", collected);

    // for c in chars_again{
    //     print!("{}", c);
    //     if c == 'o'{
    //         println!(", world!");
    //     }
    // }
    // closures
    // let num: i32 = 5;
    // let add_num = |x: i32| x + num;
    // let new_num: i32 = add_num(7);
    // dbg!(new_num);
    
    // Number literals
    // println!("Big number is {}", 98_222_000);
    // println!("Hex is {}", 0xff);
    // println!("Octal is {}", 0o77);
    // println!("Binary {}", 0b1111_0000);
    // println!("Bytes A is {}", b'A');

    // //raw - string literal
    // let text: &str = r#"{"message" : "Rust is awesome"}"#;
    // dbg!(text);

    // let a: u8 = 0b_1010_1010;
    // let b: u8 = 0b_0101_0101;
    // println!("a is {}", a);
    // println!("b is {}", b);

    // // LOgic gates
    // println!("a in binary {:08b}", a);
    // println!("b in binary {:08b}", b);

    // println!("a AND b: {:08b}", a & b);
    // println!("a OR b: {:08b}", a | b);
    // println!("a XOR b: {:08b}", a ^ b);
    // println!("NOT b: {:08b}", !b);

    // // Bitwise operations
    // println!("a << 1: {:08b}", a << 1);
    // println!("a >> 1: {:08b}", a >> 1);
    // println!("b << 1: {:08b}", b << 1);
    // println!("b >> 1: {:08b}", b >> 1);

    // Structs
    
}
