#[derive(Debug)]
struct Rectangle{
    width: u32,
    height: u32
}

impl Rectangle{
    fn area(&self) -> u32{
        self.width * self.height
    }

    fn has_width(&self) -> bool{
        self.width > 0
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

fn main() {

    let rect1 = Rectangle {
        width: 30,
        height: 50
    };

    let rect2 = Rectangle {
        width: 15,
        height: 25
    };

    println!("Area of rect1: {}",rect1.area());
    println!("Does rect1 have width: {}", rect1.has_width());
    println!("Can rect1 hold rect2: {}", rect1.can_hold(&rect2));


    println!("Hello, world!");
}
