// struct User {
//     active: bool,
//     username: String,
//     email: String,
//     sign_in_count: u64,
// }

struct Rectangle{
    width: u32,
    height: u32
}

// fn build_user(email: String, username: String) -> User {
//     User {
//         active: true,
//         username: username,
//         email: email,
//         sign_in_count: 1,
//     }
// }

fn area(rectangle: &Rectangle) -> u32{
    rectangle.width * rectangle.height
}

fn main() {
    // let user1 = User {
    //     active: true,
    //     username: String::from("mtha"),
    //     email: String::from("mtha@gmail.com"),
    //     sign_in_count: 1
    // };

    // let user2 = build_user(String::from("mthatsoe@gmail.com"), String::from("mthatsoe"));

    // println!("Username 1: {:?}, Email: {:?}, Count: {:?}, Active: {:?}", user1.username, user1.email, user1.sign_in_count, user1.active);
    // println!("Username 2: {:?}, Email: {:?}, Count: {:?}, Active: {:?}", user2.username, user2.email, user2.sign_in_count, user2.active);
    let rect1 = Rectangle{
        width: 30,
        height: 50
    };

    println!("The area of the rectangle is {} square pixels", area(&rect1));
    println!("Hello, world!");
}
